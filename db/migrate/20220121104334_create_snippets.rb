class CreateSnippets < ActiveRecord::Migration[7.0]
  def change
    create_table :snippets do |t|
      t.string :name
      t.text :content
      t.string :format
      t.belongs_to :folder
      t.belongs_to :user

      t.timestamps
    end
  end
end
