class CreateVersions < ActiveRecord::Migration[7.0]
  def change
    create_table :versions do |t|
      t.text :old
      t.text :new
      t.references :snippet, null: false, foreign_key: true

      t.timestamps
    end
  end
end
