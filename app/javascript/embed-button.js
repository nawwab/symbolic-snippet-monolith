(function(){
  const embedButtons = document.querySelectorAll('.embed-button')

  embedButtons.forEach(function(button) {
    button.addEventListener('click', function() {
      const id = button.dataset.val
      const url = `http://127.0.0.1:3000/snippets/${id}/embed`
      const tag = `<script src="${url}" type="text/javascript"></script>`

      navigator.clipboard.writeText(tag).then(function() {
        alert('embed code saved on your clipboard!')
      }, function() {
        alert('error when processing embed')
      });
    })
  })
})();
