(function(){
  const text_area = document.getElementById('snippet-input');
  const preview = document.getElementById('snippet-overlay');
  const name_field = document.getElementById('name-input');

  function updateContent() {
    preview.textContent = `${text_area.value}`
    Prism.highlightAll()
  }

  function updateFormat(format) {
    preview.classList.add('lang-' + format) 
    Prism.highlightAll()
  }

  function autosize(){
    const el = this;
    const previewParent = preview.parentElement
    setTimeout(function(){
      el.style.height = 'auto';
      el.style.height = el.scrollHeight + 'px';
      previewParent.style.height = 'auto';
      previewParent.style.height = el.scrollHeight + 'px';
    },0);
  }

  function getFormat(name) {
    const strArr = name.split('.').filter(str => str.length > 0)
    return strArr.at(-1)
  }

  if (name_field.value) {
    updateFormat(getFormat(name_field.value));
  } else {
    updateFormat('js');
  }
  
  if (text_area.value){
    updateContent();
  }

  text_area.addEventListener('keydown', autosize);

  text_area.addEventListener("input", function() {
    updateContent();
  })

  name_field.addEventListener("change", function() {
    preview.classList = ''
    updateFormat(getFormat(this.value));
  })
})();
