class VersionsController < ApplicationController
  authorize_resource except: [:embed_style, :embed_script]
  layout 'dashboard'

  def index
    @folders = Folder.accessible_by(current_ability)
    @snippet = Snippet.accessible_by(current_ability).find(params[:snippet_id])
    @versions = @snippet.versions
  end
end
