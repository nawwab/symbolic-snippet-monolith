class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
  include ApplicationHelper

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to not_found_path
  end

  rescue_from ActiveRecord::RecordNotFound do |exception|
    redirect_to not_found_path
  end

  def after_sign_in_path_for(resource)
    if resource.is_a?(User)
      if current_user.folders.exists?
        folder_snippets_path(current_user.folders.first)
      else
        folders_path
      end
    else
      admin_dashboard_path(resource)
    end
  end

  protected
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:username, :name])
  end
end
