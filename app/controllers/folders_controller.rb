class FoldersController < ApplicationController
  authorize_resource
  layout 'dashboard'

  # no show
  # index only show the list of folder
  def index
    @folders = Folder.accessible_by(current_ability)
  end

  def new
    @folders = Folder.accessible_by(current_ability)
    @folder = @folders.new
  end

  def create
    @folder = Folder.accessible_by(current_ability).new(folder_params)

    if @folder.save
      redirect_to folder_snippets_path(@folder)
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    @folders = Folder.accessible_by(current_ability)
    @folder = @folders.find(params[:id])
  end

  def update
    @folder = Folder.accessible_by(current_ability).find(params[:id])

    if @folder.update(folder_params)
      redirect_to folder_snippets_path(@folder)
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @folder = Folder.accessible_by(current_ability).find(params[:id])
    @folder.destroy
    
    if @folders.exists?
      redirect_to folder_snippets_path(@folders.first), status: :see_other
    else
      redirect_to folders_path, status: :see_other
    end
  end

  private
  def folder_params
    params.require(:folder).permit(:name, :description, :status)
  end
end
