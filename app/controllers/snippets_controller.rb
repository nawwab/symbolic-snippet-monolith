class SnippetsController < ApplicationController
  authorize_resource except: [:embed_style, :embed_script]
  layout "dashboard"
  protect_from_forgery except: [:download, :embed, :embed_style, :embed_script]
  
  # no show, instead use versions index
  def index
    @folders = Folder.accessible_by(current_ability)
    @current_folder = Folder.find(params[:folder_id])
    @snippets = @current_folder.snippets
  end

  def new
    @folders = Folder.accessible_by(current_ability)
    @folder = @folders.find(params[:folder_id])
    @snippet = @folder.snippets.new
  end

  def create
    @folder = Folder.accessible_by(current_ability).find(params[:folder_id])
    @snippet = @folder.snippets.new(snippet_params)
    @snippet.user_id = current_user.id
    unless @snippet.name[0] == '.'
      @snippet.format = @snippet.name.split('.').reject do |str|
        str.empty?
      end.last
    end

    if @snippet.save
      redirect_to folder_snippets_path(@folder)
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    @folders = Folder.accessible_by(current_ability)
    @snippet = Snippet.accessible_by(current_ability).find(params[:id])
  end

  def update
    @snippet = Snippet.accessible_by(current_ability).find(params[:id])
    @old_snippet_content = @snippet.content

    unless @snippet.name[0] == '.'
      @snippet.format = @snippet.name.split('.').reject do |str|
        str.empty?
      end.last
    end

    if @snippet.update(snippet_params)
      unless @snippet.name[0] == '.'
        format = @snippet.name.split('.').reject do |str|
          str.empty?
        end.last
        @snippet.update(format: format)
      end

      if !@old_snippet_content.eql? @snippet.content
        @snippet.versions.create(
          old: @old_snippet_content, 
          new: @snippet.content
        )
      end

      redirect_to folder_snippets_path(@snippet.folder)
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @snippet = Snippet.accessible_by(current_ability).find(params[:id])
    @snippet.destroy

    redirect_to folder_snippets_path(@snippet.folder), status: :see_other
  end

  def download
    @snippet = Snippet.accessible_by(current_ability).find(params[:id])
    send_data @snippet.content, :filename => @snippet.name
  end

  def embed
    @snippet = Snippet.accessible_by(current_ability).find(params[:id])
    
    render 'snippets/embed', handlers: :erb, formats: :js
  end

  def embed_script
    render file: "#{Rails.root}/app/javascript/embed.js", content_type: 'text/javascript;charset=UTF-8'
  end

  def embed_style
    render file: "#{Rails.root}/app/assets/stylesheets/embed.css", content_type: 'text/css;charset=UTF-8'
  end

  private
  def snippet_params
    params.require(:snippet).permit(:name, :content)
  end
end
