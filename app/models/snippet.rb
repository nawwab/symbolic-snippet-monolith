class Snippet < ApplicationRecord
  has_many :versions, dependent: :destroy
  belongs_to :user
  belongs_to :folder

  validates :name, presence: true, length: {minimum: 2}
  validates :content, presence: true
  validates :format, presence: false
end
