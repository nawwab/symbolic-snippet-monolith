# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in) soon...
    if user.is_a? AdminUser
      if user.super?
        can :manage, AdminUser
      elsif user.regular?
        can :read, AdminUser
      end
      can :create, User
      can :read, User
      can :read, Folder
      can :read, Snippet
      can :read, Version
      can :read, ActiveAdmin::Page, name: "Dashboard", namespace_name: "admin"
    elsif user.is_a? User
      if user.new_record?
        can :read, Snippet do |snippet|
          snippet.folder.is_public?
        end
        can :download, Snippet
      else
        can :create, Folder
        can :create, Snippet
        can :create, Version
        can :read, User, id: user.id
        can :read, Folder, user_id: user.id
        can :read, Snippet
        can :read, Version
        can :update, User, user_id: user.id
        can :update, Folder, user_id: user.id
        can :update, Snippet, user_id: user.id
        can :destroy, User, user_id: user.id
        can :destroy, Folder, user_id: user.id
        can :destroy, Snippet, user_id: user.id
        can :destroy, Version, user_id: user.id
        can :download, Snippet
        can :embed, Snippet
      end
    end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
