class Folder < ApplicationRecord
  belongs_to :user
  has_many :snippets, dependent: :destroy

  enum status: [ :public, :private ], _prefix: 'is'

  validates :name, presence: true, length: {minimum: 2}
  validates :description, presence: false
  validates :status, presence: true

  scope :on_public, -> { where(status: :public) }
  scope :on_private, -> { where(status: :private) }
end
