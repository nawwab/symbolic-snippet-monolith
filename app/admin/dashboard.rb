# frozen_string_literal: true
ActiveAdmin.register_page "Dashboard" do
  menu priority: 1, label: proc { I18n.t("active_admin.dashboard") }

  content title: proc { I18n.t("active_admin.dashboard") } do
    # div class: "blank_slate_container", id: "dashboard_default_message" do
    #   span class: "blank_slate" do
    #     # span I18n.t("active_admin.dashboard_welcome.welcome")
    #     span "Coming Soon"
    #   end
    # end
   columns do 
     panel "Counter" do
       resource_counter_wrapper [User, Folder, Snippet]
     end

     panel "Informasi" do
       para "Selamat datang di Dashboard Admin Symbolic Snippet", style: "font-weight: bold;"
       para "Seperti yang dilihat pada menu, terdapat beberapa menu seperti"
       ul do
         li do
           strong "Dashboard:"
           text_node "Berisi introduksi dan grafik (coming soon)."
         end
         li do
           strong "Admin Users:"
           text_node "Pengaturan seluruh admin berada di menu ini."
         end
         li do
           strong "Resource:"
           text_node "Seluruh data/resource yang dipakai oleh aplikasi dan diakses oleh pengguna diatur di menu ini."
         end
       end
     end
   end
  end # content
end
