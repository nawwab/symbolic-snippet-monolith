ActiveAdmin.register Folder do
  permit_params :name, :description, :user_id
  menu parent: 'Resource', priority: 2, icon: :folder

  scope :all, default: true
  scope :on_public 
  scope :on_private

  index do
    column 'Folder', :folder do |folder|
      link_to folder.name, admin_folder_path(folder)
    end
    column :description
    column 'Author', :user
    column :created_at
    column :updated_at
    actions do |folder|
      unless folder.snippets.empty?
        item 'See Snippets', admin_snippets_path('q[folder_id_eq]' => folder)
      end
    end
  end

  filter :user, as: :select
  filter :name, as: :string
  filter :created_at, as: :date_range
  filter :updated_at, as: :date_range
end
