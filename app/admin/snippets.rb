ActiveAdmin.register Snippet do
  # Uncomment all parameters which should be permitted for assignment
  # permit_params :name, :content, :format, :folder_id, :user_id
  
  menu parent: 'Resource', priority: 3, icon: :note

  index do
    column 'Snippet', :snippet do |snippet|
      link_to snippet.name, admin_snippet_path(snippet)
    end
    column 'Author', :user
    column 'Folder', :folder
    column :created_at
    column :updated_at
    actions do |snippet|
      unless snippet.versions.empty?
        item 'See Version', admin_versions_path('q[snippet_id_eq]' => snippet)
      end
    end
  end

  filter :user, as: :select
  filter :name, as: :string
  filter :content, as: :string
  filter :created_at, as: :date_range
  filter :updated_at, as: :date_range
end
