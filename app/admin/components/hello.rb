module Admin
  module Components
    class Hello < Arbre::Component
      builder_method :hello

      def build(attributes = {})
        super(attributes)
        text_node('Hello Makan Nasi')
        add_class('hello-component')
      end

      def tag_name
        'h1'
      end
    end
  end
end
