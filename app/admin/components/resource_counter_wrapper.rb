module Admin
  module Components
    class ResourceCounterWrapper < Arbre::Component
      builder_method :resource_counter_wrapper

      def build(resources_arr)
        resources_arr.each do |resource|
          div class: "resource_counter" do
            para resource.all.length, class: "resource_counter_number"
            para resource.name, class: "resource_counter_name"
          end
        end
      end

      def tag_name
        'div'
      end
    end
  end
end
