ActiveAdmin.register User do
  # Uncomment all parameters which should be permitted for assignment
  # permit_params :email, :encrypted_password, :username, :name, :reset_password_token, :reset_password_sent_at, :remember_created_at, :sign_in_count, :current_sign_in_at, :last_sign_in_at, :current_sign_in_ip, :last_sign_in_ip, :confirmation_token, :confirmed_at, :confirmation_sent_at, :unconfirmed_email, :failed_attempts, :unlock_token, :locked_at
  
  menu parent: 'Resource', priority: 1, icon: :person

  index do
    column 'User', :user do |user|
      link_to user.name, admin_user_path(user)
    end
    column :email
    column :username
    column :created_at
    column :updated_at
    column :last_sign_in_at
    actions do |user|
      unless user.folders.empty?
        item 'See Folders', admin_folders_path('q[user_id_eq]' => user)
      end
      unless user.snippets.empty?
        item 'See Snippets', admin_snippets_path('q[user_id_eq]' => user)
      end
    end
  end

  form do |f|
    f.inputs do
      f.input :email
      f.input :name
      f.input :username
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end

  show do
    tabs do
      tab 'Basic' do
        attributes_table :email, :username, :name, :created_at
      end
      tab 'Advanced' do
        attributes_table :sign_in_count, :current_sign_in_at, :last_sign_in_at, :current_sign_in_ip, :last_sign_in_ip, :confirmed_at, :confirmation_sent_at, :unconfirmed_email, :locked_at
      end
    end
  end

  filter :email, as: :string
  filter :username, as: :string
  filter :name, as: :string
  filter :created_at, as: :date_range
  filter :updated_at, as: :date_range
end
