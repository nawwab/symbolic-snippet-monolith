ActiveAdmin.register Version do
  # Uncomment all parameters which should be permitted for assignment
  # permit_params :old, :new, :snippet_id
  
  menu parent: 'Resource', priority: 4, icon: :fact_check
  
  index do
    id_column
    column :snippet
    column :new
    column 'Author', :version do |version|
      link_to version.snippet.user.name, admin_user_path(version.snippet.user)
    end
    column :created_at
    actions
  end

  filter :new, as: :string
  filter :created_at, as: :date_range
end
