Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root "pages#landing"
  get '/404', to: 'pages#not_found', as: 'not_found'

  # uncomment this if you use custom controller in /controllers/users
  # devise_for :users, controllers: {
  #   confirmations: 'users/confirmations',
  #   passwords: 'users/passwords',
  #   registrations: 'users/registrations',
  #   sessions: 'users/sessions',
  #   unlocks: 'users/unlocks',
  # }
  devise_for :users
  
  resources :folders, except: [:show] do
    resources :snippets, only: [:index, :new, :create]
  end

  resources :snippets, only: [:edit, :update, :destroy] do
    resources :versions, only: :index
  end

  get '/snippets/:id/file', to: 'snippets#download', as: 'download' 
  get '/snippets/:id/embed', to: 'snippets#embed', as: 'embed' 
  get '/embed/script', to: 'snippets#embed_script' 
  get '/embed/style', to: 'snippets#embed_style' 
end
